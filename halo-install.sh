#!/bin/bash

#########################################################
#							#
# Title Halo Installer Arch Custom Edition ( HIACE )	#
#							#
# Author: Brennen Murphy				#
# Date: 05/17/2018					#
# Contact: brennenmurph@pm.me                           #
# GitLab: https://gitlab.com/krazynez			#
#_______________					#					
# 		|					#
# Version 1.05	|					#
#		|					#
#########################################################


function main() {
clear


echo "All-in-one installer for Halo Custom Edition Installer for Arch Linux"
echo ""
read -p "Press enter to continue"

check
}

function check() {
clear

#Check if files exist 

if [[ -f "halocesetup_en_1.00.exe" ]]; then
	rm halocesetup_en_1.00.exe
fi

if [[ -f "haloce_patch-1.0.10.exe" ]]; then
	rm haloce-patch-1.0.10.exe
fi





#Make Multilib in pacman.conf enabled
checkComment=$(grep -F '#[multilib]' /etc/pacman.conf)
if [[ $checkComment == "#[multilib]" ]]; then
	if [[ $EUID == 0 ]]; then
		# create pacman.conf backup
		cp /etc/pacman.conf /etc/pacman.conf.bak

		#Uncomments multiilib in pacman.conf for wine 32bit libray files
		sed -i -e 's/#\[multilib]/\[multilib]/' /etc/pacman.conf


		#Uncomments the 94th line in pacman.conf -> Inclue = /etc/pacman.d/mirrorlist
		sed -i '94 s/#//' /etc/pacman.conf
		#awk -v commentId='#' /etc/pacman.conf
		#sleep 10;
	else
		echo "Multilib already uncommented. Good Job!"
	fi
fi

#Check dependecies
if ! [[ -f "/usr/bin/wget" ]]; then
	if [[ $EUID == 0 ]]; then
		sudo pacman -S --noconfirm wget
	else
		echo "Re-run as root... To install Dependencies"
		sleep 3
		exit 1;
	fi
fi
	
if ! [[ -f "/usr/bin/wine" ]]; then
	if [[ $EUID == 0 ]]; then
	sudo pacman -S --noconfirm wine-staging
       	sudo pacman -S --noconfirm winetricks
	else
		echo "Re-run as root... To install Dependencies"
		sleep 3
		exit 1;
	fi
fi

if ! [[ -f "/usr/bin/winetricks" ]]; then
	if [[ $EUID == 0 ]]; then
		sudo pacman -S --noconfirm winetricks
	else
		echo "Re-run as root... To install Dependencies"
		sleep 3
		exit 1;
	fi

else 
	clear
	echo ""
	echo "Dependancies met"
	sleep 2;

fi


if [[ $EUID == 0 ]]; then
	clear
	echo ""
	echo "Please re-run not as root"
	sleep 2;
	exit 0;
fi

install

} #END OF CHECK FUNCTION








function install() {
clear

#read -p "Please enter your username: " nonRootUser
#echo ""
#echo "Password needed for user: "
#su -p  $nonRootUser


# Downloads Halo Custom Edition Setup ##### THIS SEEMS TO CHANGE A LOT


read -p "Do you have the 'halocesetup_en_1.00.exe' already? y/n: " haloceDownloadCheck

if [[ "$haloceDownloadCheck" == "n" ]]; then

    wget  -O halocesetup_en_1.00.exe "http://download.fileplanet.com/ftp1/052004/HaloCESetup_en_1.00.exe?st=uAM3gFb-xqYIrahU_S_F7Q&e=1596214551"

    check=$(shasum halocesetup_en_1.00.exe | cut -b 1-40)

	if ! [[ "$check" == "8b8f75f93f1584d18707ce1b4330e98d34e2bedc" ]]; then
		clear
		echo "Download failed... Check URL/Network Connection"
		echo ""
		echo "URL might have changed go here to download manually: http://hce.halomaps.org/?fid=410"
		sleep 2;
		exit 1;
	fi
fi
# Downloads Halo Patch Update
wget "http://halo.bungie.net/images/games/halopc/patch/110/haloce-patch-1.0.10.exe"


# Install Needed Wine DLL's

winetricks msxml4
clear
echo ""
echo "You just need to say next to everything here no need to fill out anything."
read -p "Press enter to continue after you install..."
winetricks mfc42
clear
echo ""
echo "This part is in German just click the left button"
sleep 2;
read -p "Press enter to continue after install..."
# Installing Setup
wine halocesetup_en_1.00.exe 
clear
echo "Enter Your Product Key. If you "lost" your product key a search engine is useful also youtube."
echo ""
read -p "Press enter after setup is complete..."
# Installing Patch
clear
echo "Press CTRL + X after patch has been applied."
wine haloce-patch-1.0.10.exe


sleep 2;

rm halocesetup_en_1.00.exe
rm haloce-patch-1.0.10.exe
cd ~/






makeShortcut

} #END OF INSTALL FUNCTION



function makeShortcut() {

	echo '#!/bin/bash 
	clear
	cd ".wine/drive_c/Program Files (x86)/Microsoft Games/Halo Custom Edition"
	wine haloce.exe
	exit 0;' | tee ~/halo
	cd
	chmod a+x halo
	clear
	echo "You should be able to run Halo Custom Edition Now in terminal with: ./halo in your home directory"
	exit 0;


}




main
